import pandas as pd
import pickle
import numpy as np
from os import listdir
from os.path import isfile, join
import os
import shutil
fluccs_data_dir = "/home/choi/workD/2.fluccs_project/python_data/"
bug_data_dir = "/home/choi/workD/4.data_directory/fluccs_lang_data/"


def main():

    for count in range(1, 66):
        if not os.path.exists(fluccs_data_dir + "method_result/train_data_raw/"):
            os.makedirs(fluccs_data_dir + "method_result/train_data_raw/")
            os.makedirs(fluccs_data_dir + "method_result/train_data_csv/")

        files_location = fluccs_data_dir + "output/lang_" + str(count) + "_b/test_cases/coverage"
        file_name_list = [f for f in listdir(files_location) if isfile(join(files_location, f))]

        file_exist_check = fluccs_data_dir + "method_result/method_train_data_csv/method_lang_info_" + str(count) + "_b_data.csv"
        if isfile(file_exist_check):
            print("============= PASS GENERATE TRAIN DATA " + str(count) + "=============")
            continue

        print("============= START GENERATE TRAIN DATA " + str(count) + "=============")


        pass_coverage_list = []
        fail_coverage_list = []

        stmt_mth_id = pd.read_csv(fluccs_data_dir+"stmt_files/stmt_mth_id_Lang_"+str(count)+".csv", header=None)
        stmt_mth_id = pd.DataFrame(stmt_mth_id).sort_values([0, 1])
        sort_stmt_mth_id = stmt_mth_id.reset_index(drop=True)
        process_total_count = len(file_name_list)
        process_count = 0
        for file_name in file_name_list:
            process_count = process_count + 1

            data_frame = pd.read_json(files_location + '/' + file_name, typ="series")
            result = []
            for key in data_frame.keys():
                if key in ['testcaseName', 'testcaseResult']:
                    value = data_frame[key]
                    result.append((key, value, value))
                    continue
                for k, v in data_frame[key].items():
                    result.append((key, int(k), int(v)))

            sort_df_result = pd.DataFrame(result).sort_values([0, 1])
            coverage_data = sort_df_result.reset_index(drop=True)

            coverage_list = coverage_data[2].tolist()

            coverage_data[0] = sort_stmt_mth_id[2]
            class_list_info = coverage_data[:-2]
            class_list = class_list_info[[0, 2]]
            class_group_by = class_list.groupby([0], as_index=False).sum()
            class_group_by.loc[class_group_by[2] > 1, 2] = 1

            merged_list = class_group_by[2].tolist()
            merged_list.append(coverage_list[-1])

            if coverage_list[-1] == 0:
                pass_coverage_list.append(merged_list)
            elif coverage_list[-1] == 1:
                fail_coverage_list.append(merged_list)

            process_percent = (process_count/process_total_count)*100
            print("processing... ", round(process_percent, 2), "% ===== Lang", count, "=====")

        np_pass_coverage_list = np.asarray(pass_coverage_list)
        np_fail_coverage_list = np.asarray(fail_coverage_list)

        np.save(fluccs_data_dir + "method_result/train_data_raw/pass_train_lang_" + str(count) + "_b_data", np_pass_coverage_list)
        np.save(fluccs_data_dir + "method_result/train_data_raw/fail_train_lang_" + str(count) + "_b_data", np_fail_coverage_list)

        class_group_by.to_csv(fluccs_data_dir + "method_result/train_data_csv/method_lang_info_" + str(count) + "_b_data.csv", header=False)


if __name__ == '__main__':
    main()
