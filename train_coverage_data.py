import pickle
import tensorflow as tf
import numpy as np
import pandas as pd
import timeit
import os

# from sklearn.metrics import *
from os.path import isfile, join


fluccs_data_dir = "/home/choi/workD/2.fluccs_project/python_data/"
bug_data_dir = "/home/choi/workD/4.data_directory/fluccs_lang_data/"


##read train, result
def main():
    for number in range(1, 66):
        number = str(number)
        # if (isfile(fluccs_data_dir + "result/train_result/lang_" + number + "_b_train_result.csv")):
        #     print("============= PASS TRAIN DATA " + number + "=============")
        #     continue

        print("============= START TRAIN DATA " + number + "=============")
        data_x_path = fluccs_data_dir + "result/train_data_raw/lang_" + number + "_b_train_x.txt"
        data_y_path = fluccs_data_dir + "result/train_data_raw/lang_" + number + "_b_train_y.txt"
        data_x, data_y = loadData(data_x_path, data_y_path)
        runDnn(data_x, data_y, number)


def loadData(data_x, data_y):
    train_x_path = data_x
    train_y_path = data_y

    with open(train_x_path, "rb") as fp:
        train_x_data = pickle.load(fp)

    with open(train_y_path, "rb") as fp:
        train_y_data = pickle.load(fp)

    return np.array(train_x_data), np.array(train_y_data)


def normalize(value):
    x_min = min(value)
    x_max = max(value)
    return (value - x_min) / (x_max - x_min)


def runDnn(x_data, y_data, number):
    if not os.path.exists(fluccs_data_dir + "result/train_ckpt/lang_" + number):
        os.makedirs(fluccs_data_dir + "result/train_ckpt/lang_" + number)
    start = timeit.default_timer()

    x_data = x_data
    y_data = y_data

    len_x_data = len(x_data[0])
    test_x = np.eye(len_x_data)

    X = tf.placeholder(tf.float32)
    Y = tf.placeholder(tf.float32)

    labels = tf.placeholder(tf.float32)
    predicitions = tf.placeholder(tf.float32)

    W1 = tf.Variable(tf.random_uniform([len_x_data, 256], -1., 1.))
    W2 = tf.Variable(tf.random_uniform([256, 1], -1., 1.))

    b1 = tf.Variable(tf.zeros([256]))
    b2 = tf.Variable(tf.zeros([1]))

    L1 = tf.add(tf.matmul(X, W1), b1)
    L1 = tf.nn.relu(L1)

    model = tf.add(tf.matmul(L1, W2), b2)
    classes_weights = tf.constant([0.1, 1.0])
    cost = tf.reduce_mean(tf.nn.weighted_cross_entropy_with_logits(targets=Y, logits=model, pos_weight=classes_weights))

    optimizer = tf.train.AdamOptimizer(learning_rate=0.01)
    train_op = optimizer.minimize(cost)

    acc, acc_op = tf.metrics.accuracy(labels=labels, predictions=predicitions)
    rec, rec_op = tf.metrics.recall(labels=labels, predictions=predicitions)
    pre, pre_op = tf.metrics.precision(labels=labels, predictions=predicitions)
    saver = tf.train.Saver()
    init = tf.global_variables_initializer()
    init_l = tf.local_variables_initializer()

    sess = tf.Session()
    sess.run(init)
    sess.run(init_l)
    for step in range(100):
        _, loss_val = sess.run([train_op, cost], feed_dict={X: x_data, Y: y_data})
        print(len(np.where(y_data == 0)[0]))

        if (step + 1) % 10 == 0:
            print(step + 1)
            prediction_eval = sess.run(tf.nn.sigmoid(model), feed_dict={X: x_data})
            prediction_threshold = np.where(prediction_eval > 0.5, 1, 0)

            accuracy = sess.run(acc_op, feed_dict={labels: y_data, predicitions: prediction_threshold})  # accuracy
            recall = sess.run(rec_op, feed_dict={labels: y_data, predicitions: prediction_threshold})  # recall
            precision = sess.run(pre_op, feed_dict={labels: y_data, predicitions: prediction_threshold})  # precision
            f1 = 2 * (precision * recall) / (precision + recall)
            print("accuracy :", accuracy)
            print("recall :", recall)
            print("precision :", precision)
            print("f1 score :", round(f1, 2))
            print("loss : ", loss_val)


        if (step + 1) % 100 == 0:
            ckpt_path = saver.save(sess, fluccs_data_dir + "result/train_ckpt/lang_" + number + "/train_" + number, step + 1)
            print("save ckpt file:", ckpt_path)
    end = timeit.default_timer()
    print(end - start)

    prediction = sess.run(tf.nn.sigmoid(model), feed_dict={X: test_x})
    df = pd.DataFrame(prediction)
    df.to_csv(fluccs_data_dir + "result/train_result/lang_" + number + "_b_train_result.csv", header=False)

if __name__ == "__main__":
    main()
            # print("Precision", precision_score(y_data, prediction_threshold))
            # print("Recall", recall_score(y_data, prediction_threshold))
            # print("f1_score", f1_score(y_data, prediction_threshold))
            # print("confusion_matrix")
