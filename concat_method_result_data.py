import pandas as pd

# import os
# if not os.path.exists(directory):
#     os.makedirs(directory)
import os
fluccs_data_dir = "/home/choi/workD/2.fluccs_project/python_data/"
bug_data_dir = "/home/choi/workD/4.data_directory/fluccs_lang_data/"


def main():
    if not os.path.exists(fluccs_data_dir + "method_result/total_result_data"):
        os.makedirs(fluccs_data_dir + "method_result/total_result_data")
    for number in range(1, 66):
        number = str(number)

        line_data = read_line_data(number)
        suspicion_data = read_train_data(number)
        line_suspicion_data = merge_line_and_train_data(line_data, suspicion_data)
        try:
            faulty_data = read_faulty_data(number)
        except (pd.errors.EmptyDataError, FileNotFoundError):
            continue
        total_data = merge_faulty_and_suspicion_data(faulty_data, line_suspicion_data)
        print(total_data)
        save_total_data_csv(number, total_data)

def save_total_data_csv(number, total_data):
    totoal_result_csv = pd.DataFrame(total_data.loc[total_data["faulty_flag"] == 1])
    totoal_result_csv.to_csv(fluccs_data_dir + "method_result/total_result_data/lang_" + number + "_b_total_result_data.csv", index=False)


def merge_faulty_and_suspicion_data(faulty_data, line_suspicion_data):
    merged_faulty_suspiciouness_data = pd.merge(line_suspicion_data, faulty_data, how="left",
                                                on=["class_name"])
    print(line_suspicion_data)
    print(faulty_data)
    merged_faulty_suspiciouness_data["faulty_flag"].fillna(0, inplace=True)
    total_result = merged_faulty_suspiciouness_data.sort_values(["suspiciousness"], ascending=False)
    # print(total_result)
    total_result = total_result.reset_index(drop=True)
    total_result["rank"] = total_result.index
    total_result["percentile_rank"] = total_result.index / len(total_result)
    return total_result


def read_faulty_data(number):
    faulty_info_path = fluccs_data_dir + "method_result/patch_data_csv/" + number + "_lang_fault.csv"
    faulty_info = pd.read_csv(faulty_info_path, header=None)
    faulty_info.columns = ["class_name", "faulty_flag"]
    # faulty_info.line_number = faulty_info.line_number.astype(int)
    return faulty_info


def merge_line_and_train_data(line_info, train_result):
    # print(len(line_info))
    # print(len(train_result))
    result = pd.merge(line_info, train_result, on=0)
    # print(result)
    result = result.drop(columns=[2])
    # print(result)
    header = ["rank", "class_name", "suspiciousness"]
    result.columns = header
    # result.line_number = result.line_number.astype(int)
    return result


def read_train_data(number):
    train_result_path = fluccs_data_dir + "method_result/train_result/lang_" + number + "_b_train_result_500_1000_layer1_1000.csv"
    train_result = pd.read_csv(train_result_path, header=None)
    return train_result


def read_line_data(number):
    line_info_path = fluccs_data_dir + "method_result/train_data_csv/method_lang_info_" + number + "_b_data.csv"
    line_info = pd.read_csv(line_info_path, header=None)
    return line_info


if __name__ == '__main__':
    main()
