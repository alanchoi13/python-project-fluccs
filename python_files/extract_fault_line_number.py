from os import listdir
from os.path import isfile, join
import pandas as pd


def main():
    files = [f for f in listdir("patch_data_raw") if isfile(join("patch_data_raw", f))]
    for file in files:
        contents = read_files(file)
        class_name, line_number, start_number = extract_line_info(contents)
        fault_list = extract_falut_lines(class_name, contents, line_number, start_number)
        file_name = file.replace(".src.patch", "_lang_fault.csv")
        fault_dataframe = pd.DataFrame(fault_list)
        fault_dataframe.to_csv("result/patch_data_raw/" + file_name, header=None, index=False)


def test():
    file = "4.src.patch"
    contents = read_files(file)
    fault_list = extract_line_info(contents)
    file_name = file.replace(".src.patch", "_lang_fault.csv")
    fault_dataframe = pd.DataFrame(fault_list)
    fault_dataframe.to_csv("result/patch_data_raw/" + file_name, header=None, index=False)


def extract_falut_lines(class_name, contents, line_numbers, start_numbers):
    if(isinstance(start_numbers, list) and isinstance(line_numbers, list)) :
        fault_list = []

        for start, line_number in zip(start_numbers, line_numbers):
            for line in contents[start:]:
                if ("+   " in line):
                    continue

                if ("-   " in line):
                    fault_list.append([class_name, line_number])

                    line_number = int(line_number) + 1
    else:
        fault_list = []

        for line in contents[start_numbers:]:
            if ("+   " in line):
                continue

            if ("-   " in line):
                fault_list.append([class_name, line_numbers])

            line_numbers = int(line_numbers) + 1

    return fault_list


def read_files(file):


    with open("patch_data_raw/"+file, encoding="utf-8") as f:
        contents = f.readlines()
    contents = [x.strip() for x in contents]
    return contents

def extract_line_info(contents):
    class_name = ""
    line_number = 0
    fault_list = []
    count = 0

    for line in contents:

        if ("--- a" in line):
            start = line.find("java/") + 5
            end = line.find(".java", start)
            sub_class = line[start:end]
            class_name = sub_class.replace("/", ".")

        if (("@@ " in line)):
            count = 0
            start = line.find("@@ ") + 3
            end = line.find(" @@", start)
            sub_line = line[start:end]
            sub_start = sub_line.find("-") + 1
            sub_end = sub_line.find(",", sub_start)
            line_number = int(sub_line[sub_start:sub_end])

        if ("+   " in line):
            continue

        if ("-   " in line):
            line_numbers = line_number + count
            fault_list.append([class_name, line_numbers-1, 1])

        count = count + 1
    print(fault_list)

    return fault_list



if __name__ == '__main__':
    main()
    # test()
