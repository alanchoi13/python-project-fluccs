import os, sys, getopt
import numpy as np
import pandas as pd
from os.path import isfile, join

fluccs_data_dir = "/home/choi/workD/2.fluccs_project/python_data/"

project_name = ''
version = ''
working_dir = ''


def execute():
    argv_info(sys.argv[1:])
    print(project_name, version, working_dir)
    # project_name = "Closure"
    # version = "1"
    # working_dir = ""

    statement_info = read_stmt_info()
    coverage_files = read_coverage_files()

    pass_coverage_list = []
    fail_coverage_list = []

    process_total_count = len(coverage_files)
    process_count = 0

    for file_name in coverage_files:
        process_count = process_count + 1

        refined_data = refine_json_data(pd.read_json(file_name, typ="series"))
        coverage_data = pd.DataFrame(refined_data).sort_values([0, 1]).reset_index(drop=True)
        coverage_list = coverage_data[2].tolist()

        coverage_data[0] = statement_info[2]
        class_list_info = coverage_data[:-2]
        class_list = class_list_info[[0, 2]]
        class_group_by = class_list.groupby([0], as_index=False).sum()
        class_group_by.loc[class_group_by[2] > 1, 2] = 1

        merged_list = class_group_by[2].tolist()
        merged_list.append(coverage_list[-1])

        if coverage_list[-1] == 0:
            pass_coverage_list.append(merged_list)
        elif coverage_list[-1] == 1:
            fail_coverage_list.append(merged_list)

        print(np.asarray(pass_coverage_list).shape)
        print(np.asarray(fail_coverage_list).shape)

        process_percent = (process_count / process_total_count) * 100
        print("processing... ", round(process_percent, 2), "% ===== Lang", version, "=====")

    np.save(fluccs_data_dir + "method_result/train_data_raw/pass_train_lang_" + str(version) + "_b_data",
            np.asarray(pass_coverage_list))
    np.save(fluccs_data_dir + "method_result/train_data_raw/fail_train_lang_" + str(version) + "_b_data",
            np.asarray(fail_coverage_list))

    class_group_by.to_csv(
        fluccs_data_dir + "method_result/train_data_csv/method_lang_info_" + str(version) + "_b_data.csv", header=False)


def get_env(name):
    return os.environ[name]


def get_pwd():
    return os.getcwd()


def print_argvinfo(sys):
    print("Number of arguments:", len(sys.argv), 'arguments.')
    print("Argument List : ", str(sys.argv))


def generate_np_data(count):
    if check_folder(count) != 1:
        return

    pass_coverage_list = []
    fail_coverage_list = []

    file_name_list, files_location = read_coverage_files(count)

    process_total_count = len(file_name_list)
    process_count = 0

    sort_stmt_mth_id = read_stmt_info(count)

    for file_name in file_name_list:
        process_count = process_count + 1

        data_frame = pd.read_json(files_location + '/' + file_name, typ="series")
        result = refine_json_data(data_frame)

        sort_df_result = pd.DataFrame(result).sort_values([0, 1])
        coverage_data = sort_df_result.reset_index(drop=True)
        coverage_list = coverage_data[2].tolist()

        coverage_data[0] = sort_stmt_mth_id[2]
        class_list_info = coverage_data[:-2]
        class_list = class_list_info[[0, 2]]
        class_group_by = class_list.groupby([0], as_index=False).sum()
        class_group_by.loc[class_group_by[2] > 1, 2] = 1

        merged_list = class_group_by[2].tolist()
        merged_list.append(coverage_list[-1])

        if coverage_list[-1] == 0:
            pass_coverage_list.append(merged_list)
        elif coverage_list[-1] == 1:
            fail_coverage_list.append(merged_list)

        process_percent = (process_count / process_total_count) * 100
        print("processing... ", round(process_percent, 2), "% ===== Lang", count, "=====")

    np.save(fluccs_data_dir + "method_result/train_data_raw/pass_train_lang_" + str(count) + "_b_data",
            np.asarray(pass_coverage_list))
    np.save(fluccs_data_dir + "method_result/train_data_raw/fail_train_lang_" + str(count) + "_b_data",
            np.asarray(fail_coverage_list))

    class_group_by.to_csv(
        fluccs_data_dir + "method_result/train_data_csv/method_lang_info_" + str(count) + "_b_data.csv", header=False)


def refine_json_data(data_frame):
    refine_data = []
    for key in data_frame.keys():
        if key in ['testcaseName', 'testcaseResult']:
            value = data_frame[key]
            refine_data.append((key, value, value))
            continue
        for k, v in data_frame[key].items():
            refine_data.append((key, int(k), int(v)))
    return refine_data


def read_coverage_files():
    # path = get_pwd()
    path = "/home/choi/workD/checkout_data/"
    working_dir = "closure_2_b"
    coverage_path = path + working_dir + "/test_cases/coverage/"
    file_names = [f for f in os.listdir(coverage_path) if isfile(join(coverage_path, f))]
    print("============= START GENERATE TRAIN DATA " + str(version) + "=============")
    coverage_files = [coverage_path + file_name for file_name in file_names]
    return coverage_files


def read_stmt_info():
    path = "/framework/bin/fluccs/method_stmt/"
    stmt_path = get_env('D4J_HOME') + path + project_name + "/stmt_mth_id_" + project_name + "_" + str(version) + ".csv"
    stmt_mth_id = pd.read_csv(stmt_path, header=None)
    stmt_mth_id = pd.DataFrame(stmt_mth_id).sort_values([0, 1])
    sort_stmt_mth_id = stmt_mth_id.reset_index(drop=True)
    return sort_stmt_mth_id


def check_folder(count):
    if not os.path.exists(fluccs_data_dir + "method_result/train_data_raw/"):
        os.makedirs(fluccs_data_dir + "method_result/train_data_raw/")
        os.makedirs(fluccs_data_dir + "method_result/train_data_csv/")
    if isfile(fluccs_data_dir + "method_result/method_train_data_csv/method_lang_info_" + str(count) + "_b_data.csv"):
        print("============= PASS GENERATE TRAIN DATA " + str(count) + "=============")
        return 0
    else:
        return 1


def argv_info(argv):
    global project_name
    global version
    global working_dir

    try:
        opts, args = getopt.getopt(argv, "hp:v:w:m:", ["project=", "version=", "workdir="])
    except getopt.GetoptError as err:
        print(str(err))
        sys.exit(1)
    get_pwd()
    for opt, arg in opts:

        if opt == "-p":
            print("project :", arg)
            project_name = arg
            # argv_list.append(arg)
        elif opt == "-v":
            print("version :", arg)
            # argv_list.append(arg)
            version = arg
        elif opt == "-w":
            print("working Directory :", arg)
            working_dir = arg
            # argv_list.append(arg)
        elif opt == "-m":
            # argv_list.append(arg)
            print("method : ", arg)
        elif opt == "-h":
            print("choose help")


if __name__ == '__main__':
    execute()
    # print(get_env("D4J_HOME"))
    # print_argvinfo(sys)
    # print(get_pwd())
