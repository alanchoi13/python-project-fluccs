import os, sys, getopt
import numpy as np
import pandas as pd
from os.path import isfile, join
import subprocess

fluccs_data_dir = "/home/choi/workD/2.fluccs_project/python_data/"
bug_data_dir = "/home/choi/workD/4.data_directory/fluccs_lang_data/"




class ExtractStatementFaulty:
    def __init__(self):
        self.project_name = None
        self.version = None
        self.working_dir = None
        self.project_name_lower = None

        self.d4j_env = _get_env("D4J_HOME")
        self.stmt_path = "/framework/bin/fluccs/method_stmt/"
        self.pwd = _get_pwd()
        self.pass_coverage_list = []
        self.fail_coverage_list = []
        self.class_group_by = []
        self.type = "/method_data/"

    def set_argv(self, argv):
        try:
            opts, args = getopt.getopt(argv, "hp:v:w:m:", ["project=", "version=", "workdir="])
        except getopt.getopterror as err:
            print(str(err))
            sys.exit(1)
        for opt, arg in opts:

            if opt == "-p":
                print("project :", arg)
                self.project_name = arg
                self.project_name_lower = arg.lower()
                # argv_list.append(arg)
            # elif opt == "-v":
            #     print("version :", arg)
            #     # argv_list.append(arg)
            #     self.version = arg
            # elif opt == "-w":
            #     print("working directory :", arg)
            #     self.working_dir = arg
            #     # argv_list.append(arg)
            elif opt == "-m":
                # argv_list.append(arg)
                print("method : ", arg)
            elif opt == "-h":
                print("choose help")

    def execute_statements(self):
        self.pwd = "/home/choi/workD/checkout_data"
        base_path_d4j = self.d4j_env + "/framework/projects/"+self.project_name
        base_path_pwd = self.pwd + "/output/" + self.project_name + self.type

        files = [f for f in os.listdir(base_path_d4j + "/patches") if isfile(join(base_path_d4j + "/patches", f))]
        for file in files:
            bug_id = extract_bid_in_filename(file)
            bug_data_dir = self.pwd + "/" + self.project_name_lower + '_' + bug_id + "_b"
            commit_bug_hash_id, commit_fix_hash_id = extract_commit_db_info(bug_id, base_path_d4j)
            extracted_filename = extract_filename(read_files(file,base_path_d4j))
            bugged_file_path = extract_file_path(commit_bug_hash_id, extracted_filename, bug_id, bug_data_dir)
            fixed_file_path = extract_file_path(commit_fix_hash_id, extracted_filename, bug_id, bug_data_dir)
            git_diff_info = extract_git_diff_info(commit_bug_hash_id, commit_fix_hash_id, bugged_file_path,
                                                  fixed_file_path,
                                                  bug_id, bug_data_dir)
            save_patch_raw_data(file, git_diff_info, base_path_pwd)
            diff_info_lines = split_lines(git_diff_info)
            fault_list = extract_line_info(diff_info_lines)
            if not fault_list:
                continue
            save_csv_name = file.replace(".src.patch", "_lang_fault.csv")
            save_patch_csv_data(fault_list, save_csv_name, base_path_pwd)


def main():
    execute_statements()


def execute_statements():

    files = [f for f in os.listdir(fluccs_data_dir + "patches") if isfile(join(fluccs_data_dir + "patches", f))]
    for file in files:
        bug_id = extract_bid_in_filename(file)
        commit_bug_hash_id, commit_fix_hash_id = extract_commit_db_info(bug_id)
        extracted_filename = extract_filename(read_files(file))
        bugged_file_path = extract_file_path(commit_bug_hash_id, extracted_filename, bug_id)
        fixed_file_path = extract_file_path(commit_fix_hash_id, extracted_filename, bug_id)
        git_diff_info = extract_git_diff_info(commit_bug_hash_id, commit_fix_hash_id, bugged_file_path, fixed_file_path,
                                              bug_id)
        save_patch_raw_data(file, git_diff_info)
        diff_info_lines = split_lines(git_diff_info)
        fault_list = extract_line_info(diff_info_lines)
        if not fault_list:
            continue
        save_csv_name = file.replace(".src.patch", "_lang_fault.csv")
        save_patch_csv_data(fault_list, save_csv_name)


def save_patch_csv_data(fault_list, save_name, base_path_pwd):
    fault_dataframe = pd.DataFrame(fault_list)
    fault_dataframe.to_csv(base_path_pwd + "patch_data_csv/" + save_name, header=None, index=False)


def save_patch_raw_data(file, git_diff_info, base_path_pwd):
    opened_file = open(base_path_pwd + "patch_data_raw/" + file, 'w')
    print(git_diff_info)
    opened_file.write(git_diff_info)
    opened_file.close()


def extract_bid_in_filename(file):
    patch_name = file.split(".")
    bid = patch_name[0]
    return bid


def extract_commit_db_info(bid, base_path):
    cmd = "grep \"^" + str(bid) + ",\" " + base_path + "/commit-db"
    output = subprocess.check_output(cmd, shell=True, )
    output = output.decode("ISO-8859-1")
    rev_id = output.split(',')[1:]
    rev_id = [x.strip() for x in rev_id]
    return rev_id


def extract_filename(contents):
    count = 0
    for line in contents:
        count = count + 1
        if "diff" in line:
            contents = line.split("/", )
            for content in contents:
                if ".java" in content:
                    outputs = content.split(" ")
                    for output in outputs:
                        if "java" in output:
                            print(output)
                            return output


def read_files(file, base_path):
    with open(base_path + "/patches/" + file, encoding="ISO-8859-1") as f:
        contents = f.readlines()
    contents = [x.strip() for x in contents]
    return contents


def split_lines(lines):
    lines = lines.splitlines()
    lines = [x.strip() for x in lines]
    return lines


def extract_file_path(hash_id, file_name, bid, bug_data_dir):
    print(bug_data_dir)
    # git ls - tree - r - -name - only 87 b2e62b7c6e81cd9d5c872b7fa9cc8fd3f1509 | grep NumberUtils.java
    cmd = "git ls-tree -r --name-only " + hash_id + " | grep " + file_name
    output = subprocess.check_output(cmd, shell=True, cwd=bug_data_dir)
    output = output.decode("ISO-8859-1")
    output = output.strip()
    return output


def extract_git_diff_info(bug_id, fix_id, bug_file_location, fix_file_location, bid, bug_data_dir):
    cmd = "git diff -u " + bug_id + ":" + bug_file_location + " " + fix_id + ":" + fix_file_location
    output = subprocess.check_output(cmd, shell=True, cwd=bug_data_dir)
    output = output.decode("ISO-8859-1")
    output = output.strip()
    return output


def extract_line_info(contents):
    class_name = ""
    line_number = 0
    fault_list = []
    count = 0

    for line in contents:

        if ("--- a" in line):
            start = line.find("java/") + 5
            end = line.find(".java", start)
            sub_class = line[start:end]
            class_name = sub_class.replace("/", ".")

        if (("@@ " in line)):
            count = 0
            start = line.find("@@ ") + 3
            end = line.find(" @@", start)
            sub_line = line[start:end]
            sub_start = sub_line.find("-") + 1
            sub_end = sub_line.find(",", sub_start)
            line_number = int(sub_line[sub_start:sub_end])

        if ("+   " in line):
            continue

        if ("-   " in line):
            line_numbers = line_number + count
            fault_list.append([class_name, line_numbers - 1, 1])

        count = count + 1
    return fault_list

def _get_env(name):
    return os.environ[name]


def _get_pwd():
    return os.getcwd()


if __name__ == '__main__':
    esf = ExtractStatementFaulty()
    esf.set_argv(sys.argv[1:])
    esf.execute_statements()

    # main()
    # test()
