import os, sys, getopt
import numpy as np
import pandas as pd
from os.path import isfile, join
fluccs_data_dir =""

class ExtractMethodFaultyData:
    def __init__(self, proj, ver, workdir):
        self.project_name = proj
        self.version = ver
        self.working_dir = workdir
        self.d4j_env = _get_env("D4J_HOME")
        self.stmt_path = "/framework/bin/fluccs/method_stmt/"
        self.pwd = _get_pwd()
        self.pass_coverage_list = []
        self.fail_coverage_list = []
        self.class_group_by = []
        self.type = "/method_data/"


    def merge_faulty_files(self):

        basic_path = self.pwd + "/output/" + self.project_name + self.type
        stmt_save_path = basic_path + "stmt_data/stmt_" + self.project_name + "_" + self.version + "_data.csv"


        if not os.path.exists(basic_path + "faulty_data/"):
            os.makedirs(basic_path + "faulty_data/")

        # for number in range(1, 66):
        number = self.version
        number = str(number)

        # try:
        faulty_info = pd.read_csv(basic_path + "result/patch_data_csv/" + str(number) + "_lang_fault.csv",
                                  header=None)
        stmt_info = pd.read_csv(basic_path + "stmt_files/stmt_mth_id_Lang_" + str(number) + ".csv",
                                header=None)
        # except FileNotFoundError:
        #     continue

        # faulty_info, stmt_info = read_file()
        # print(csv1, csv2)
        faulty_info.columns = ["class_name", "line_number", "faulty_flag"]
        stmt_info.columns = ["class_name", "line_number", "method_name"]

        print(faulty_info)
        print(stmt_info)

        result = pd.merge(stmt_info, faulty_info, on=["class_name", "line_number"])
        result = result.drop(columns=["class_name", "line_number"])
        print(result)

        result = result.drop_duplicates(["method_name"])
        print(result)
        result.to_csv(fluccs_data_dir + "method_result/patch_data_csv/" + str(number) + "_lang_fault.csv",
                      header=None, index=False)


def _get_env(name):
    return os.environ[name]


def _get_pwd():
    return os.getcwd()

def _argv_info(argv):
    try:
        opts, args = getopt.getopt(argv, "hp:v:w:m:", ["project=", "version=", "workdir="])
    except getopt.getopterror as err:
        print(str(err))
        sys.exit(1)
    # get_pwd()
    project_name = ''
    version = ''
    working_dir = ''

    for opt, arg in opts:

        if opt == "-p":
            print("project :", arg)
            project_name = arg
            # argv_list.append(arg)
        elif opt == "-v":
            print("version :", arg)
            # argv_list.append(arg)
            version = arg
        elif opt == "-w":
            print("working directory :", arg)
            working_dir = arg
            # argv_list.append(arg)
        elif opt == "-m":
            # argv_list.append(arg)
            print("method : ", arg)
        elif opt == "-h":
            print("choose help")

    return project_name, version, working_dir

# def merge_faulty_files():
#     if not os.path.exists(fluccs_data_dir + "method_result/patch_data_csv"):
#         os.makedirs(fluccs_data_dir + "method_result/patch_data_csv")
#     for number in range(1, 66):
#         number = str(number)
#
#         try :
#             faulty_info = pd.read_csv(fluccs_data_dir + "result/patch_data_csv/"+str(number)+"_lang_fault.csv", header=None)
#             stmt_info = pd.read_csv(fluccs_data_dir + "stmt_files/stmt_mth_id_Lang_"+str(number)+".csv", header=None )
#         except FileNotFoundError:
#             continue
#
#         # faulty_info, stmt_info = read_file()
#         # print(csv1, csv2)
#         faulty_info.columns = ["class_name", "line_number", "faulty_flag"]
#         stmt_info.columns = ["class_name", "line_number", "method_name"]
#
#         print(faulty_info)
#         print(stmt_info)
#
#         result = pd.merge(stmt_info, faulty_info , on=["class_name", "line_number"])
#         result = result.drop(columns=["class_name", "line_number"])
#         print(result)
#
#         result = result.drop_duplicates(["method_name"])
#         print(result)
#         result.to_csv(fluccs_data_dir + "method_result/patch_data_csv/"+str(number)+"_lang_fault.csv", header=None, index=False)


if __name__ == '__main__':
    project_name, version, working_dir = _argv_info(sys.argv[1:])
    efd = ExtractMethodFaultyData(project_name, version, working_dir)
    efd.merge_faulty_files()
    # merge_faulty_files()
    # pass