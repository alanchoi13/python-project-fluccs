import os, sys, getopt
import numpy as np
import pandas as pd
from os.path import isfile, join
# import os.mkdir

fluccs_data_dir = "/home/choi/workd/2.fluccs_project/python_data/"

project_name = ''
version = ''
working_dir = ''


class GenerateMethodData:
    def __init__(self, proj, ver, workdir):
        self.project_name = proj
        self.version = ver
        self.working_dir = workdir
        self.d4j_env = _get_env("D4J_HOME")
        self.stmt_path = "/framework/bin/fluccs/method_stmt/"
        self.pwd = _get_pwd()
        self.pass_coverage_list = []
        self.fail_coverage_list = []
        self.class_group_by = []
        self.type = "/method_data/"

    def read_statement_info(self):
        path = self.stmt_path
        stmt_path = self.d4j_env + path + self.project_name + "/stmt_mth_id_" + self.project_name + "_" + str(
            self.version) + ".csv"
        stmt_mth_id = pd.read_csv(stmt_path, header=None)
        stmt_mth_id = pd.DataFrame(stmt_mth_id).sort_values([0, 1])
        sort_stmt_mth_id = stmt_mth_id.reset_index(drop=True)
        return sort_stmt_mth_id

    def read_coverage_files(self):
        path = self.pwd
        # path = "/home/choi/workD/checkout_data/"
        # coverage_path = path+ "/" + self.working_dir + "/test_cases/coverage/"
        coverage_path = path+ "/" + self.working_dir + "/test_cases/coverage/"

        file_names = [f for f in os.listdir(coverage_path) if isfile(join(coverage_path, f))]
        print("============= start generate train data " + str(version) + "=============")
        coverage_files = [coverage_path + file_name for file_name in file_names]
        return coverage_files

    def generate_np_data(self, statement_info, coverage_files):

        base_path_pwd = self.pwd + "/output/" + self.project_name + self.type

        if not os.path.exists(base_path_pwd + "npy_data/original/"):
            os.makedirs(base_path_pwd + "npy_data/original/")

        if not os.path.exists(base_path_pwd + "stmt_data/"):
            os.makedirs(base_path_pwd + "stmt_data/")

        pass_save_path = base_path_pwd + "npy_data/original/pass_" + self.project_name + "_" + self.version + "_data"
        fail_save_path = base_path_pwd + "npy_data/original/fail_" + self.project_name + "_" + self.version + "_data"
        stmt_save_path = base_path_pwd + "stmt_data/stmt_" + self.project_name + "_" + self.version + "_data.csv"

        process_total_count = len(coverage_files)
        process_count = 0

        for file_name in coverage_files:
            process_count = process_count + 1

            refined_data = _refine_json_data(pd.read_json(file_name, typ="series"))
            coverage_data = pd.DataFrame(refined_data).sort_values([0, 1]).reset_index(drop=True)
            coverage_list = coverage_data[2].tolist()

            coverage_data[0] = statement_info[2]
            class_list_info = coverage_data[:-2]
            class_list = class_list_info[[0, 2]]
            self.class_group_by = class_list.groupby([0], as_index=False).sum()
            self.class_group_by.loc[self.class_group_by[2] > 1, 2] = 1

            merged_list = self.class_group_by[2].tolist()
            merged_list.append(coverage_list[-1])

            if coverage_list[-1] == 0:
                self.pass_coverage_list.append(merged_list)
            elif coverage_list[-1] == 1:
                self.fail_coverage_list.append(merged_list)

            print(np.asarray(self.pass_coverage_list).shape)
            print(np.asarray(self.fail_coverage_list).shape)

            process_percent = (process_count / process_total_count) * 100
            print("processing... ", round(process_percent, 2), "% ===== Lang", version, "=====")

        np.save(pass_save_path, np.asarray(self.pass_coverage_list))
        np.save(fail_save_path, np.asarray(self.fail_coverage_list))

        self.class_group_by.to_csv(stmt_save_path, header=False)


def _get_env(name):
    return os.environ[name]


def _get_pwd():
    return os.getcwd()


def _refine_json_data(data_frame):
    refine_data = []
    for key in data_frame.keys():
        if key in ['testcaseName', 'testcaseResult']:
            value = data_frame[key]
            refine_data.append((key, value, value))
            continue
        for k, v in data_frame[key].items():
            refine_data.append((key, int(k), int(v)))
    return refine_data


def check_folder(count):
    if not os.path.exists(fluccs_data_dir + "method_result/train_data_raw/"):
        os.makedirs(fluccs_data_dir + "method_result/train_data_raw/")
        os.makedirs(fluccs_data_dir + "method_result/train_data_csv/")
    if isfile(fluccs_data_dir + "method_result/method_train_data_csv/method_lang_info_" + str(count) + "_b_data.csv"):
        print("============= pass generate train data " + str(count) + "=============")
        return 0
    else:
        return 1


def _argv_info(argv):
    try:
        opts, args = getopt.getopt(argv, "hp:v:w:m:", ["project=", "version=", "workdir="])
    except getopt.getopterror as err:
        print(str(err))
        sys.exit(1)
    # get_pwd()
    project_name = ''
    version = ''
    working_dir = ''

    for opt, arg in opts:

        if opt == "-p":
            print("project :", arg)
            project_name = arg
            # argv_list.append(arg)
        elif opt == "-v":
            print("version :", arg)
            # argv_list.append(arg)
            version = arg
        elif opt == "-w":
            print("working directory :", arg)
            working_dir = arg
            # argv_list.append(arg)
        elif opt == "-m":
            # argv_list.append(arg)
            print("method : ", arg)
        elif opt == "-h":
            print("choose help")

    return project_name, version, working_dir


if __name__ == '__main__':
    project_name, version, working_dir = _argv_info(sys.argv[1:])
    gmd = GenerateMethodData(project_name, version, working_dir)
    gmd.generate_np_data(gmd.read_statement_info(), gmd.read_coverage_files())
