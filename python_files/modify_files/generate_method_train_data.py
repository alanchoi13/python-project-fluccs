import numpy as np
import pandas as pd
import os, sys, getopt
from os.path import isfile, join

fluccs_data_dir = "/home/choi/workD/2.fluccs_project/python_data/"
# bug_data_dir = "/home/choi/workD/4.data_directory/fluccs_lang_data/"


class GenerateMethodNpyData:
    def __init__(self, proj, ver, workdir):
        self.d4j_env = _get_env("D4J_HOME")
        self.pwd = _get_pwd()
        self.project_name = proj
        self.version = ver
        self.working_dir = workdir
        self.type = "/method_data/"
        self.sample_number = 1000


    def read_npy(self):
        # pwd = self.pwd
        self.pwd = "/home/choi/workD/checkout_data"
        basic_path = self.pwd + "/output/" + self.project_name + self.type
        if not os.path.exists(basic_path + "npy_data/custom/"):
            os.makedirs(basic_path + "npy_data/custom/")

        pass_xy = np.load(
            basic_path + "npy_data/original/pass_" + self.project_name + "_" + self.version + "_data.npy")
        fail_xy = np.load(
            basic_path + "npy_data/original/fail_" + self.project_name + "_" + self.version + "_data.npy")

        print_nparray_shape(pass_xy)
        print_nparray_shape(fail_xy)
        print(fail_xy)

        print(pass_xy)

        pass_xy = random_sampling(pass_xy, self.sample_number)

        print(pass_xy.shape)

        weight = get_data_ratio(fail_xy, pass_xy)

        fail_list = covert_nparray_to_list(fail_xy)
        pass_list = covert_nparray_to_list(pass_xy)

        w_fail_list = fail_list * weight

        merged_xy_list = merged_lists(pass_list, w_fail_list)

        merged_xy_np = covert_list_to_nparray(merged_xy_list)
        print_nparray_shape(merged_xy_np)

        shuffle_nparray(merged_xy_np)

        np.save(basic_path + "npy_data/custom/merged_" + self.project_name + "_" + str(self.version) + "_data_"+ str(self.sample_number), merged_xy_np)

def _get_env(name):
    return os.environ[name]


def _get_pwd():
    return os.getcwd()


def extract_count():
    for number in range(1, 66):
        number = str(number)
        print("================= LANG ", number)
        pass_xy = np.load(fluccs_data_dir + "method_result/train_data_raw/pass_train_lang_" + number + "_b_data.npy")
        fail_xy = np.load(fluccs_data_dir + "method_result/train_data_raw/fail_train_lang_" + number + "_b_data.npy")
        print_nparray_shape(pass_xy)
        print_nparray_shape(fail_xy)

        sample_number = 700


        print_nparray_0_len(pass_xy)
        print_nparray_0_len(fail_xy)

        pass_xy = pass_xy[np.random.choice(pass_xy.shape[0], sample_number, replace=False)]
        print(pass_xy)
        pass_xy = random_sampling(pass_xy, sample_number)

        print(pass_xy.shape)

        weight = get_data_ratio(fail_xy, pass_xy)

        fail_list = covert_nparray_to_list(fail_xy)
        pass_list = covert_nparray_to_list(pass_xy)

        w_fail_list = fail_list * weight

        merged_xy_list = merged_lists(pass_list, w_fail_list)

        merged_xy_np = covert_list_to_nparray(merged_xy_list)
        print_nparray_shape(merged_xy_np)

        shuffle_nparray(merged_xy_np)

        np.save(fluccs_data_dir + "method_result/train_data_raw/basic_train_lang_" + number + "_b_data_"+str(sample_number),
                merged_xy_np)

        # print(pass_xy.tolist())
def read_npy():

    # for number in range(1,66):
    for number in range(1, 66):
        number = str(number)
        print("================= LANG ",number)
        pass_xy = np.load(fluccs_data_dir + "method_result/train_data_raw/pass_train_lang_" + number + "_b_data.npy")
        fail_xy = np.load(fluccs_data_dir + "method_result/train_data_raw/fail_train_lang_" + number + "_b_data.npy")

        print_nparray_shape(pass_xy)
        print_nparray_shape(fail_xy)

        weight = get_data_ratio(fail_xy, pass_xy)

        fail_list = covert_nparray_to_list(fail_xy)
        pass_list = covert_nparray_to_list(pass_xy)

        w_fail_list = fail_list * weight

        w_fail_np = covert_list_to_nparray(w_fail_list)

        # print_nparray_shape(w_fail_np)

        merged_xy_list = merged_lists(pass_list, w_fail_list)

        merged_xy_np = covert_list_to_nparray(merged_xy_list)
        print_nparray_shape(merged_xy_np)

        # print_2d_array_last_v(merged_xy_np)

        shuffle_nparray(merged_xy_np)
        # print_2d_array_last_v(merged_xy_np)

        np.save(fluccs_data_dir + "method_result/train_data_raw/basic_train_lang_" + number + "_b_data_ratio", merged_xy_np)

        # print(pass_xy.tolist())


def random_sampling(pass_xy, sample_number):
    pass_xy = pass_xy[np.random.choice(pass_xy.shape[0], sample_number, replace=False)]
    return pass_xy


def covert_nparray_to_list(fail_xy):
    fail_list = fail_xy.tolist()
    return fail_list


def print_2d_array_last_v(merged_xy_np):
    print(merged_xy_np[:, -1])


def shuffle_nparray(merged_xy_np):
    np.random.shuffle(merged_xy_np)


def merged_lists(pass_list, w_fail_list):
    merged_xy_list = pass_list + w_fail_list
    return merged_xy_list


def covert_list_to_nparray(w_fail_list):
    w_fail_np = np.asarray(w_fail_list)
    return w_fail_np


def print_nparray_shape(pass_xy):
    print(pass_xy.shape)


def get_data_ratio(fail_xy, pass_xy):
    if pass_xy.shape[0] > fail_xy.shape[0]:
        weight = int(pass_xy.shape[0] / fail_xy.shape[0])
    else:
        weight = int(fail_xy.shape[0] / pass_xy.shape[0])

    print(weight)
    return weight


def print_nparray_0_len(pass_xy):
    print(pass_xy.shape[0])

def _argv_info(argv):
    try:
        opts, args = getopt.getopt(argv, "hp:v:w:m:", ["project=", "version=", "workdir="])
    except getopt.getopterror as err:
        print(str(err))
        sys.exit(1)
    # get_pwd()
    project_name = ''
    version = ''
    working_dir = ''

    for opt, arg in opts:

        if opt == "-p":
            print("project :", arg)
            project_name = arg
            # argv_list.append(arg)
        elif opt == "-v":
            print("version :", arg)
            # argv_list.append(arg)
            version = arg
        elif opt == "-w":
            print("working directory :", arg)
            working_dir = arg
            # argv_list.append(arg)
        elif opt == "-m":
            # argv_list.append(arg)
            print("method : ", arg)
        elif opt == "-h":
            print("choose help")

    return project_name, version, working_dir


if __name__ == '__main__':
    project_name, version, working_dir = _argv_info(sys.argv[1:])
    gen_npy = GenerateMethodNpyData(project_name, version, working_dir)
    gen_npy.read_npy()
    # # read_npy()
    # extract_count()
