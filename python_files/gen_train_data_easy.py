import numpy as np
import pandas as pd
import os, sys, getopt
from os.path import isfile, join
# from python_files.test_gen_method_data import *

fluccs_data_dir = "/home/choi/workD/2.fluccs_project/python_data/"
# bug_data_dir = "/home/choi/workD/4.data_directory/fluccs_lang_data/"



# def GenerateMethodData:

def read_npy(number):

    # for number in range(1,66):
    # for number in range(1, 66):
    # number = str(number)
    print("================= LANG ",number)
    pass_xy = np.load(fluccs_data_dir + "result/train_data_raw/pass_train_lang_" + number + "_b_data.npy")
    fail_xy = np.load(fluccs_data_dir + "result/train_data_raw/fail_train_lang_" + number + "_b_data.npy")

    # print_nparray_shape(pass_xy)
    # print_nparray_shape(fail_xy)
    #
    # print_nparray_0_len(pass_xy)
    # print_nparray_0_len(fail_xy)

    weight = get_data_ratio(fail_xy, pass_xy)

    fail_list = covert_nparray_to_list(fail_xy)
    pass_list = covert_nparray_to_list(pass_xy)

    w_fail_list = fail_list * weight

    w_fail_np = covert_list_to_nparray(w_fail_list)

    # print_nparray_shape(w_fail_np)

    merged_xy_list = merged_lists(pass_list, w_fail_list)

    merged_xy_np = covert_list_to_nparray(merged_xy_list)
    print_nparray_shape(merged_xy_np)

    # print_2d_array_last_v(merged_xy_np)

    shuffle_nparray(merged_xy_np)
    # print_2d_array_last_v(merged_xy_np)

    np.save(fluccs_data_dir + "result/train_data_raw/basic_train_lang_" + number + "_b_data_ratio", merged_xy_np)

        # print(pass_xy.tolist())


def covert_nparray_to_list(fail_xy):
    fail_list = fail_xy.tolist()
    return fail_list


def print_2d_array_last_v(merged_xy_np):
    print(merged_xy_np[:, -1])


def shuffle_nparray(merged_xy_np):
    np.random.shuffle(merged_xy_np)


def merged_lists(pass_list, w_fail_list):
    merged_xy_list = pass_list + w_fail_list
    return merged_xy_list


def covert_list_to_nparray(w_fail_list):
    w_fail_np = np.asarray(w_fail_list)
    return w_fail_np


def print_nparray_shape(pass_xy):
    print(pass_xy.shape)


def get_data_ratio(fail_xy, pass_xy):
    if pass_xy.shape[0] > fail_xy.shape[0]:
        weight = int(pass_xy.shape[0] / fail_xy.shape[0])
    else:
        weight = int(fail_xy.shape[0] / pass_xy.shape[0])

    print(weight)
    return weight


def print_nparray_0_len(pass_xy):
    print(pass_xy.shape[0])


def _get_env(name):
    return os.environ[name]


def _get_pwd():
    return os.getcwd()


if __name__ == '__main__':
    read_npy()
