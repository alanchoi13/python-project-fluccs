import pandas as pd
import numpy as np
from os import listdir
from os.path import isfile, join
import os

fluccs_data_dir = "/home/choi/workD/2.fluccs_project/python_data/"


def generate_np_data(count):

    if check_folder(count) != 1:
        return

    pass_coverage_list = []
    fail_coverage_list = []

    file_name_list, files_location = read_coverage_files(count)

    process_total_count = len(file_name_list)
    process_count = 0

    sort_stmt_mth_id = read_stmt_info(count)


    for file_name in file_name_list:
        process_count = process_count + 1

        data_frame = pd.read_json(files_location + '/' + file_name, typ="series")
        result = refine_json_data(data_frame)

        sort_df_result = pd.DataFrame(result).sort_values([0, 1])
        coverage_data = sort_df_result.reset_index(drop=True)
        coverage_list = coverage_data[2].tolist()

        coverage_data[0] = sort_stmt_mth_id[2]
        class_list_info = coverage_data[:-2]
        class_list = class_list_info[[0, 2]]
        class_group_by = class_list.groupby([0], as_index=False).sum()
        class_group_by.loc[class_group_by[2] > 1, 2] = 1

        merged_list = class_group_by[2].tolist()
        merged_list.append(coverage_list[-1])

        if coverage_list[-1] == 0:
            pass_coverage_list.append(merged_list)
        elif coverage_list[-1] == 1:
            fail_coverage_list.append(merged_list)

        process_percent = (process_count/process_total_count)*100
        print("processing... ", round(process_percent, 2), "% ===== Lang", count, "=====")


    np.save(fluccs_data_dir + "method_result/train_data_raw/pass_train_lang_" + str(count) + "_b_data",
            np.asarray(pass_coverage_list))
    np.save(fluccs_data_dir + "method_result/train_data_raw/fail_train_lang_" + str(count) + "_b_data",
            np.asarray(fail_coverage_list))

    class_group_by.to_csv(fluccs_data_dir + "method_result/train_data_csv/method_lang_info_" + str(count) + "_b_data.csv", header=False)


def refine_json_data(data_frame):
    result = []
    for key in data_frame.keys():
        if key in ['testcaseName', 'testcaseResult']:
            value = data_frame[key]
            result.append((key, value, value))
            continue
        for k, v in data_frame[key].items():
            result.append((key, int(k), int(v)))
    return result


def read_coverage_files(count):
    files_location = fluccs_data_dir + "output/lang_" + str(count) + "_b/test_cases/coverage"
    file_name_list = [f for f in listdir(files_location) if isfile(join(files_location, f))]
    print("============= START GENERATE TRAIN DATA " + str(count) + "=============")
    return file_name_list, files_location


def read_stmt_info(count):
    ## read statement information
    stmt_mth_id = pd.read_csv(fluccs_data_dir + "stmt_files/stmt_mth_id_Lang_" + str(count) + ".csv", header=None)
    stmt_mth_id = pd.DataFrame(stmt_mth_id).sort_values([0, 1])
    sort_stmt_mth_id = stmt_mth_id.reset_index(drop=True)
    return sort_stmt_mth_id


def check_folder(count):
    if not os.path.exists(fluccs_data_dir + "method_result/train_data_raw/"):
        os.makedirs(fluccs_data_dir + "method_result/train_data_raw/")
        os.makedirs(fluccs_data_dir + "method_result/train_data_csv/")
    if isfile(fluccs_data_dir + "method_result/method_train_data_csv/method_lang_info_" + str(count) + "_b_data.csv"):
        print("============= PASS GENERATE TRAIN DATA " + str(count) + "=============")
        return 0
    else:
        return 1


# def check_folder(count):



if __name__ == '__main__':
    pass
    # generate_np_data()
