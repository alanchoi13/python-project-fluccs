import subprocess
import pandas as pd
from os import listdir
from os.path import isfile, join

fluccs_data_dir = "/home/choi/workD/2.fluccs_project/python_data/"
bug_data_dir = "/home/choi/workD/4.data_directory/fluccs_lang_data/"

def test():
    files = [f for f in listdir(fluccs_data_dir + "patches") if isfile(join(fluccs_data_dir + "patches", f))]
    for file in files:
        bug_id = extract_bid_in_filename(file)
        commit_bug_hash_id, commit_fix_hash_id = extract_commit_db_info(bug_id)
        extracted_filename = extract_filename(read_files(file))
        bugged_file_path = extract_file_path(commit_bug_hash_id, extracted_filename, bug_id)
        fixed_file_path = extract_file_path(commit_fix_hash_id, extracted_filename, bug_id)
        git_diff_info = extract_git_diff_info(commit_bug_hash_id, commit_fix_hash_id, bugged_file_path, fixed_file_path, bug_id)

        print(git_diff_info)


def main():
    execute_statements()


def execute_statements():
    files = [f for f in listdir(fluccs_data_dir + "patches") if isfile(join(fluccs_data_dir + "patches", f))]
    for file in files:
        bid = extract_bid_in_filename(file)
        commit_bug_hash_id, commit_fix_hash_id = extract_commit_db_info(bid)
        extracted_filename = extract_filename(read_files(file))
        bugged_file_path = extract_file_path(commit_bug_hash_id, extracted_filename, bid)
        fixed_file_path = extract_file_path(commit_fix_hash_id, extracted_filename, bid)
        git_diff_info = extract_git_diff_info(commit_bug_hash_id, commit_fix_hash_id, bugged_file_path, fixed_file_path,
                                              bid)
        save_patch_raw_data(file, git_diff_info)
        diff_info_lines = split_lines(git_diff_info)
        fault_list = extract_line_info(diff_info_lines)
        if not fault_list:
            continue
        save_csv_name = file.replace(".src.patch", "_lang_fault.csv")
        save_patch_csv_data(fault_list, save_csv_name)


def save_patch_csv_data(fault_list, save_name):
    fault_dataframe = pd.DataFrame(fault_list)
    fault_dataframe.to_csv(fluccs_data_dir + "result/patch_data_csv/" + save_name, header=None, index=False)


def save_patch_raw_data(file, git_diff_info):
    opened_file = open(fluccs_data_dir + "result/patch_data_raw/" + file, 'w')
    print(git_diff_info)
    opened_file.write(git_diff_info)
    opened_file.close()


def extract_bid_in_filename(file):
    patch_name = file.split(".")
    bid = patch_name[0]
    return bid


def extract_commit_db_info(bid):
    cmd = "grep \"^" + str(bid) + ",\" " + fluccs_data_dir + "python_files/commit-db"
    output = subprocess.check_output(cmd, shell=True, )
    output = output.decode("ISO-8859-1")
    rev_id = output.split(',')[1:]
    rev_id = [x.strip() for x in rev_id]
    return rev_id


def extract_filename(contents):
    count = 0
    for line in contents:
        count = count + 1
        if ("diff" in line):
            start = line.find("java/") + 5
            end = line.find(".java", start)
            raw_line = line[start:end + 5]
            return raw_line


def read_files(file):
    with open(fluccs_data_dir + "patches/" + file, encoding="ISO-8859-1") as f:
        contents = f.readlines()
    contents = [x.strip() for x in contents]
    return contents


def split_lines(lines):
    lines = lines.splitlines()
    lines = [x.strip() for x in lines]
    return lines


def extract_file_path(hash_id, file_name, bid):
    # git ls - tree - r - -name - only 87 b2e62b7c6e81cd9d5c872b7fa9cc8fd3f1509 | grep NumberUtils.java
    cmd = "git ls-tree -r --name-only " + hash_id + " | grep " + file_name
    output = subprocess.check_output(cmd, shell=True, cwd=bug_data_dir + "lang_" + bid + "_b")
    output = output.decode("ISO-8859-1")
    output = output.strip()
    return output


def extract_git_diff_info(bug_id, fix_id, bug_file_location, fix_file_location, bid):
    cmd = "git diff -u " + bug_id + ":" + bug_file_location + " " + fix_id + ":" + fix_file_location
    output = subprocess.check_output(cmd, shell=True, cwd=bug_data_dir + "lang_" + bid + "_b")
    output = output.decode("ISO-8859-1")
    output = output.strip()
    return output


def extract_line_info(contents):
    class_name = ""
    line_number = 0
    fault_list = []
    count = 0

    for line in contents:

        if ("--- a" in line):
            start = line.find("java/") + 5
            end = line.find(".java", start)
            sub_class = line[start:end]
            class_name = sub_class.replace("/", ".")

        if (("@@ " in line)):
            count = 0
            start = line.find("@@ ") + 3
            end = line.find(" @@", start)
            sub_line = line[start:end]
            sub_start = sub_line.find("-") + 1
            sub_end = sub_line.find(",", sub_start)
            line_number = int(sub_line[sub_start:sub_end])

        if ("+   " in line):
            continue

        if ("-   " in line):
            line_numbers = line_number + count
            fault_list.append([class_name, line_numbers - 1, 1])

        count = count + 1
    return fault_list


if __name__ == '__main__':
    # main()
    test()