import numpy as np
import pandas as pd
import os
fluccs_data_dir = "/home/choi/workD/2.fluccs_project/python_data/"



def read_file():
    pass


def merge_faulty_files():
    if not os.path.exists(fluccs_data_dir + "method_result/patch_data_csv"):
        os.makedirs(fluccs_data_dir + "method_result/patch_data_csv")
    for number in range(1, 66):
        number = str(number)

        try :
            faulty_info = pd.read_csv(fluccs_data_dir + "result/patch_data_csv/"+str(number)+"_lang_fault.csv", header=None)
            stmt_info = pd.read_csv(fluccs_data_dir + "stmt_files/stmt_mth_id_Lang_"+str(number)+".csv", header=None )
        except FileNotFoundError:
            continue

        # faulty_info, stmt_info = read_file()
        # print(csv1, csv2)
        faulty_info.columns = ["class_name", "line_number", "faulty_flag"]
        stmt_info.columns = ["class_name", "line_number", "method_name"]

        print(faulty_info)
        print(stmt_info)

        result = pd.merge(stmt_info, faulty_info , on=["class_name", "line_number"])
        result = result.drop(columns=["class_name", "line_number"])
        print(result)

        result = result.drop_duplicates(["method_name"])
        print(result)
        result.to_csv(fluccs_data_dir + "method_result/patch_data_csv/"+str(number)+"_lang_fault.csv", header=None, index=False)


if __name__ == '__main__':
    merge_faulty_files()
    # pass