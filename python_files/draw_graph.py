from os import listdir
from os.path import isfile, join
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker



fluccs_data_dir = "/home/choi/workD/2.fluccs_project/python_data/"
bug_data_dir = "/home/choi/workD/4.data_directory/fluccs_lang_data/"


def main():
    percentage_list = get_files_info()
    percent_list = list(map(lambda x : x * 100, (percentage_list)))
    print(percent_list)

    draw_graph(percent_list)


def get_files_info():
    files_location = fluccs_data_dir + "result/total_result_data/"
    file_name_list = [f for f in listdir(files_location) if isfile(join(files_location, f))]
    percentage_list = []
    for file_name in file_name_list:
        print(file_name)
        csv_file = pd.read_csv(files_location + file_name)

        try:
            percentage_list.append(csv_file["percentile_rank"][0])
        except (IndexError, FileNotFoundError):
            continue
    return percentage_list


def draw_graph(percentage_list):
    x_axis_values = np.arange(0, 101, 1)
    percentage_list_len = len(percentage_list)
    y_axis_count = []
    y_axis_percentage = []

    str_x_value = []
    for x_value in x_axis_values:
        if (x_value % 10) == 0:
            str_x_value.append(str(x_value) +"%")

        result_list = np.where(percentage_list < x_value)
        y_axis_count.append(len(list(result_list[0])))
        y_axis_percentage.append(len(list(result_list[0])) / percentage_list_len)

    y_percent_list = list(map(lambda x : x * 100, (y_axis_percentage)))

    loc_10 = plticker.MultipleLocator(base=10.0)
    loc_5 = plticker.MultipleLocator(base=5.0)

    fig = plt.figure(figsize=(12, 8))
    ax = fig.add_subplot(1,1,1)
    ax.set_xlim([0, 101])
    ax.set_ylim([0, 45])
    ax.xaxis.set_major_locator(loc_10)
    ax.yaxis.set_major_locator(loc_5)



    ##Strings
    x_label = "% of executable statements that need to be examined"
    y_label = "faulty versions"
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.plot(x_axis_values, y_axis_count)
    plt.grid(True)
    plt.show()

    fig = plt.figure(figsize=(12, 8))
    ax = fig.add_subplot(1,1,1)
    ax.set_xlim([0, 101])
    ax.set_ylim([0, 101])
    ax.xaxis.set_major_locator(loc_5)
    ax.yaxis.set_major_locator(loc_5)

    x_label = "% of executable statements that need to be examined"
    y_label = "% of faulty versions"

    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.plot(x_axis_values, y_percent_list)
    plt.grid(True)
    plt.show()


if __name__ == '__main__':
    main()
