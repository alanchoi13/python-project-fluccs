import os.path
import pandas as pd
import pickle
import numpy as np
from os import listdir
from os.path import isfile, join

fluccs_data_dir = "/home/choi/workD/2.fluccs_project/python_data/"
bug_data_dir = "/home/choi/workD/4.data_directory/fluccs_lang_data/"


def main():
    for count in range(1, 66):
        # file_exist_check = fluccs_data_dir + "result/train_data_raw/" + "lang_" + str(count) + "_b_train_x.txt"
        # if isfile(file_exist_check):
        #     print("============= PASS TRAIN DATA " + str(count) + "=============")
        #     continue

        print("============= START TRAIN DATA " + str(count) + "=============")
        files_location = fluccs_data_dir + "output/lang_" + str(count) + "_b/test_cases/coverage"
        file_name_list = [f for f in listdir(files_location) if isfile(join(files_location, f))]
        generate_train_data(str(count), file_name_list, files_location)


def generate_train_data(bug_id, file_name_list, files_location):
    pass_coverage_list = []
    fail_coverage_list = []
    basic_coverage_list = []
    count = 0

    for file_name in file_name_list:
        data_frame = pd.read_json(files_location + '/' + file_name, typ="series")
        result = []
        for key in data_frame.keys():
            if key in ['testcaseName', 'testcaseResult']:
                value = data_frame[key]
                result.append((key, value, value))
                continue
            for k, v in data_frame[key].items():
                result.append((key, int(k), int(v)))

        sort_df_result = pd.DataFrame(result).sort_values([0, 1])
        coverage_data = sort_df_result.reset_index(drop=True)
        coverage_list = coverage_data[2].tolist()
        coverage_list2 = coverage_data[2].tolist()

        del coverage_list[-2]

        float_coverage_list = list(map(float, coverage_list))
        basic_coverage_list.append(float_coverage_list)

        if coverage_list[-1] == 0:
            pass_coverage_list.append(float_coverage_list)

        if coverage_list[-1] == 1:
            fail_coverage_list.append(float_coverage_list)
            print(coverage_list2[-2])
            count = count + 1

    np_pass_coverage_list = np.asarray(pass_coverage_list)
    np_pass_cover_list_down_sample = np_pass_coverage_list[np.random.choice(np_pass_coverage_list.shape[0], 1000, replace=False)]
    pass_coverage_list = np_pass_cover_list_down_sample.tolist()
    duplicate_number = 200
    duplicate_fail_list = fail_coverage_list * int(duplicate_number / count)
    pass_coverage_list += duplicate_fail_list
    np_fail_coverage_list = np.asarray(fail_coverage_list)
    total_array_list = np.asarray(pass_coverage_list)
    np.random.shuffle(total_array_list)

    np.save(fluccs_data_dir + "result/train_data_raw/pass_train_lang_" + bug_id + "_b_data", np_pass_coverage_list)
    np.save(fluccs_data_dir + "result/train_data_raw/fail_train_lang_" + bug_id + "_b_data", np_fail_coverage_list)


def append_list_test(fail_list, total_merged_list):
    np_total_merged_list = np.array(total_merged_list)
    print(np_total_merged_list.shape)
    n1 = np.array([1, 2, 3])
    n2 = np.array([[1, 2, 3, 4], [2, 3, 4, 4], [3, 4, 5, 4]])
    fail_list = fail_list * 25
    np_fail_list = np.array(fail_list)
    print(n1.shape, n2.shape, np_fail_list.shape)
    total_merged_list += fail_list
    t1_np = np.array(total_merged_list)
    print(t1_np.shape)


def load_data():
    train_x_path = "output/lang_1_b_train_x.txt"
    train_y_path = "output/lang_1_b_train_y.txt"

    with open(train_x_path, "rb") as fp:
        train_x_data = pickle.load(fp)

    with open(train_y_path, "rb") as fp:
        train_y_data = pickle.load(fp)


def test():
    pass


if __name__ == "__main__":
    main()
    test()
    # loadData()
