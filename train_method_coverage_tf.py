import numpy as np
import tensorflow as tf
# from numpy import genfromtxt
import pandas as pd
from os.path import isfile
import timeit
import os
from tensorboard import summary as summary_lib
import shutil
fluccs_data_dir = "/home/choi/workD/2.fluccs_project/python_data/"
bug_data_dir = "/home/choi/workD/4.data_directory/fluccs_lang_data/"
config = tf.ConfigProto(
        device_count = {'GPU': 0}
    )

def input_fn():
    if os.path.exists(fluccs_data_dir + "method_result/logs"):
        shutil.rmtree(fluccs_data_dir + "method_result/logs", ignore_errors=True)
    if not os.path.exists(fluccs_data_dir + "method_result/logs"):
        os.makedirs(fluccs_data_dir + "method_result/logs")
    if not os.path.exists(fluccs_data_dir + "method_result/train_result"):
        os.makedirs(fluccs_data_dir + "method_result/train_result")

    for number in range(1, 66):
        tf.reset_default_graph()
        number = str(number)
        # if not os.path.exists(fluccs_data_dir + "result/logs" + number):
        #     os.makedirs(fluccs_data_dir + "result/logs" + number)
        if not os.path.exists(fluccs_data_dir + "method_result/train_ckpt/lang_" + number):
            os.makedirs(fluccs_data_dir + "method_result/train_ckpt/lang_" + number)

        # if (isfile(fluccs_data_dir + "result/train_result/lang_" + number + "_b_train_result.csv")):
        #     print("============= PASS TRAIN DATA " + number + "=============")
        #     continue
        print("============= START TRAIN DATA " + number + "=============")



        xy = np.load(fluccs_data_dir + "method_result/train_data_raw/basic_train_lang_" + number + "_b_data_500.npy")

        x_data = xy[:, 0:-1]
        y_data = xy[:, [-1]]

        test_x = np.eye(x_data.shape[1])
        start = timeit.default_timer()
        y_int_data = y_data.astype(int)

        X = tf.placeholder(tf.float32,  name='features')
        Y = tf.placeholder(tf.float32,  name='labels')

        pre_Y = tf.placeholder(tf.float32)

        W1 = tf.Variable(tf.random_uniform([x_data.shape[1], 1600], -1., 1.), name='W1')
        b1 = tf.Variable(tf.zeros([1600]))
        L1 = tf.add(tf.matmul(X, W1), b1)
        L1 = tf.nn.relu(L1)
        #
        # W2 = tf.Variable(tf.random_uniform([512, 256], -1., 1.), name='W2')
        # b2 = tf.Variable(tf.zeros([256]))
        # L2 = tf.add(tf.matmul(L1, W2), b2)
        # L2 = tf.nn.relu(L2)

        # W3 = tf.Variable(tf.random_uniform([256, 128], -1., 1.), name="W3")
        # b3 = tf.Variable(tf.zeros([128]))
        # L3 = tf.add(tf.matmul(L2, W3), b3)
        # L3 = tf.nn.relu(L3)

        W4 = tf.Variable(tf.random_uniform([1600, 1], -1., 1.), name="W3")
        b4 = tf.Variable(tf.zeros([1]))

        model = tf.add(tf.matmul(L1, W4), b4)


        global_step = tf.Variable(0, trainable=False, name='global_step')
        loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=Y, logits=model))
        v_loss = tf.summary.scalar('loss_lang_'+number, loss)

        optimizer = tf.train.AdamOptimizer(learning_rate=0.01)
        train_op = optimizer.minimize(loss, global_step=global_step)

        _, acc_op = tf.metrics.accuracy(labels=y_int_data, predictions=pre_Y)
        _, rec_op = tf.metrics.recall(labels=y_int_data, predictions=pre_Y)
        _, pre_op = tf.metrics.precision(labels=y_int_data, predictions=pre_Y)

        f1 = 2 * pre_op * rec_op / (pre_op + rec_op)

        v_acc = tf.summary.scalar('accuracy_lang_'+number, acc_op)
        v_rec = tf.summary.scalar('recall_lang_'+number, rec_op)
        v_pre = tf.summary.scalar('precision_lang_'+number, pre_op)
        v_f1 = tf.summary.scalar('f1 score_lang_'+number, f1)

        merged = tf.summary.merge_all()

        init = tf.global_variables_initializer()
        init_l = tf.local_variables_initializer()
        # init_all = tf.initialize_all_variables()


        with tf.Session(config=config) as sess, tf.device('/cpu:0'):
            sess.run(init)
            sess.run(init_l)
            writer = tf.summary.FileWriter(fluccs_data_dir + "method_result/logs/lang", sess.graph)
            saver = tf.train.Saver()

            for step in range(1000):
                _, loss_val, pred_val = sess.run([train_op, loss, tf.nn.sigmoid(model)], feed_dict={X: x_data, Y: y_data})
                pred_th = np.where(pred_val > 0.5, 1, 0)
                acc, rec, pre, merged_list = sess.run([acc_op, rec_op, pre_op, merged], feed_dict={X: x_data, Y: y_data, pre_Y : pred_th})
                f1_v = 2 * pre * rec / (pre + rec)

                writer.add_summary(merged_list, step + 1)
                if (step + 1) % 100 == 0:
                    print("currnet step : ", step + 1)
                    print("accuracy :", acc)
                    print("recall :", rec)
                    print("precision :", pre)
                    print("f1 score : ", f1_v)
                    print("loss :", loss_val)

                    ckpt_path = saver.save(sess, fluccs_data_dir + "method_result/train_ckpt/lang_" + number + "/train_" + number, step + 1)
                    print("===== save ckpt file:", ckpt_path)

                    prediction = sess.run(tf.nn.sigmoid(model), feed_dict={X: test_x})
                    df = pd.DataFrame(prediction)
                    df.to_csv(fluccs_data_dir + "method_result/train_result/lang_" + number + "_b_train_result_500_"+ str(step+1)+"_layer1_1600.csv",
                              header=False)

            end = timeit.default_timer()
            print("Run Time", end - start)
            writer.flush()


if __name__ == "__main__":
    input_fn()
