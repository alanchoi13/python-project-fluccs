from os import listdir
from os.path import isfile, join
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
import os

d4j_home = os.getenv("D4J_HOME")

fluccs_data_dir = "/home/choi/workD/2.fluccs_project/python_data"
# bug_data_dir = "/home/choi/workD/4.data_directory/fluccs_lang_data/"


def main():
    d4j_home = os.getenv("D4J_HOME")
    # print(d4j_home)
    n_sample = str(100)
    n_samples = ["100", "300", "500", "700", "1000"]
    n_epoches = ["100","200","300","400","500","600","700","800","900","1000"]
    for n_sample in n_samples:
        for n_epoch in n_epoches:
            percentage_list = get_files_info(n_sample, n_epoch)
            percent_list = list(map(lambda x : x * 100, (percentage_list)))
            print(percent_list)
            draw_graph(percent_list, n_sample, n_epoch)



def get_files_info(n_sample, n_epoch):
    # for epoch in n_epoch
    files_location = fluccs_data_dir + "/method_result/total_result_data/sample-"+n_sample+"/epoch-"+n_epoch+"/"
    print(files_location)

    # files_location = fluccs_data_dir + "method_result/total_result_data/"
    file_name_list = [f for f in listdir(files_location) if isfile(join(files_location, f))]
    percentage_list = []
    for file_name in file_name_list:
        print(file_name)
        csv_file = pd.read_csv(files_location + file_name)

        try:
            percentage_list.append(csv_file["percentile_rank"][0])
        except (IndexError, FileNotFoundError):
            continue
    return percentage_list


def draw_graph(percentage_list, n_sample, n_epoch):
    if not os.path.exists(fluccs_data_dir + "/method_result/method_plot/sample-"+n_sample+"/"):
        os.makedirs(fluccs_data_dir + "/method_result/method_plot/sample-"+n_sample+"/")
    x_axis_values = np.arange(0, 101, 1)
    percentage_list_len = len(percentage_list)
    y_axis_count = []
    y_axis_percentage = []

    str_x_value = []
    for x_value in x_axis_values:
        if (x_value % 10) == 0:
            str_x_value.append(str(x_value) +"%")

        result_list = np.where(percentage_list < x_value)
        y_axis_count.append(len(list(result_list[0])))
        y_axis_percentage.append(len(list(result_list[0])) / percentage_list_len)

    y_percent_list = list(map(lambda x : x * 100, (y_axis_percentage)))

    loc_10 = plticker.MultipleLocator(base=10.0)
    loc_5 = plticker.MultipleLocator(base=5.0)

    fig = plt.figure(figsize=(12, 8))
    ax = fig.add_subplot(1,1,1)
    ax.set_xlim([0, 101])
    ax.set_ylim([0, 45])
    ax.xaxis.set_major_locator(loc_10)
    ax.yaxis.set_major_locator(loc_5)



    ##Strings
    x_label = "% of executable methods that need to be examined"
    y_label = "faulty versions"
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.plot(x_axis_values, y_axis_count)
    plt.grid(True)
    fig.savefig(fluccs_data_dir + "/method_result/method_plot/sample-"+n_sample+"/versions-sample-"+n_sample+"-epoch-"+n_epoch+".png")
    # plt.show()
    plt.close()

    fig = plt.figure(figsize=(12, 8))
    ax = fig.add_subplot(1,1,1)
    ax.set_xlim([0, 101])
    ax.set_ylim([0, 101])
    ax.xaxis.set_major_locator(loc_5)
    ax.yaxis.set_major_locator(loc_5)

    x_label = "% of executable methods that need to be examined"
    y_label = "% of faulty versions"

    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.plot(x_axis_values, y_percent_list)
    plt.grid(True)
    fig.savefig(fluccs_data_dir + "/method_result/method_plot/sample-"+n_sample+"/percentile-sample-"+n_sample+"-epoch-"+n_epoch+".png")
    plt.close()

    # plt.show()


if __name__ == '__main__':
    main()
