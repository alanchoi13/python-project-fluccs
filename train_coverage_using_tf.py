import tensorflow as tf
import numpy as np

tf.logging.set_verbosity(tf.logging.INFO)

fluccs_data_dir = "/home/choi/workD/2.fluccs_project/python_data/"


def input_fn():
    xy = np.load(fluccs_data_dir + 'result/train_data_raw/train_lang_1_b_data.npy')
    x_data = xy[:, 0:-1]
    y_data = xy[:, [-1]]

    features = tf.constant(x_data, tf.float32)
    label = tf.constant(y_data, tf.float32)

    return features, label


def model_fn(features, labels, mode):
    layer1 = tf.layers.dense(features, 256, tf.nn.relu)
    layer2 = tf.layers.dense(layer1, 128, tf.nn.relu)
    output = tf.layers.dense(layer2, 1)

    global_step = tf.train.get_global_step()
    loss = tf.losses.sigmoid_cross_entropy(labels, output)
    train_op = tf.train.GradientDescentOptimizer(0.01).minimize(loss, global_step)
    acc = tf.metrics.accuracy(labels, tf.round(tf.nn.sigmoid(output)))
    recall = tf.metrics.accuracy(labels, tf.round(tf.nn.sigmoid(output)))
    precision = tf.metrics.precision(labels, tf.round(tf.nn.sigmoid(output)))

    eval_metric_op = {"acc" : acc, "recall" : recall, "precision" : precision}

    return tf.estimator.EstimatorSpec(
        mode=mode,
        loss=loss,
        train_op=train_op,
        eval_metric_ops=eval_metric_op)


if __name__ == '__main__':
    est = tf.estimator.Estimator(model_fn)
    for epoch in range(10):
        est.train(input_fn)
        est.evaluate(input_fn)